# Raspberry_RTK_ZED-F9P


Recipe to use ardusimple's u-blox ZED F9P shield with Raspberry Pi 3B+  
(raspbian/linux) utilising u-blox internal real-time high precision 
position option (DGNSS). (Really recommend a good gps antenna!)

---------

Write u-blox config "config_ublox_f9p_wade_v5.3.txt" (GPL) from cerea-forum.de to the
simpleRTK2B shield (ardusimple.com). Use ms-windows (sorry) with u-center from u-blox.

(There is also the ubxtool (python) from "gpsd" and pyUblox ... ?!)

---------

Info: F9P setup is:

USB-input: UBX,NMEA,RTCM3x USB-output:NMEA (Samp: 10Hz / 100ms)

UART1: 57600,8N1 UART1-input: UBX,NMEA,RTCM3x UART1-output: non

UART2: 57600,8N1 UART1-input: RTCM3x UART2-output: non

plus some optimizations ...

---------

Connecting to Pi3B+ with this shield:
(https://www.itead.cc/wiki/RPI_Arduino_Sheild_Add-on_V2.0)

UART1-TX: D0 (Ardu) - 15 (GPIO) - 10 (PIN) 
UART1-RX: D1 (Ardu) - 14 (GPIO) - 8 (PIN) -> ttyS0 (Pi3 B+)

UART2-TX: D2 (Ardu) - 17 (GPIO) - 11 (PIN) 
UART2-RX: D8 (Ardu) - 24 (GPIO) - 18 (PIN) -> ?

-----------

apt -f install rtklib  #2.4.3+dfsg1-1

Feed RTCM3 corrections from sapos.hvbg.hessen.de/service.php via UART1 to the ZED-F9P 
shield + Arduino-RPI adapter using str2str:

`str2str -in ntrip://loginname:password@ip-of-server:2101/VRS_3_4G_HE -p 50.6 9.5 300.0 -n 1 -out serial://ttyS0:57600:8:n:1:off`

(change -p to the "lat lon high" of the VRS (virtuelle referenzstation). 
choose a position near the positions where intend to measure.)

Note: For testing - be sure gpsd is (really) out of the way !!

Do easy NMEA logging via USB-output:

`stty -F /dev/ttyACM0 57600`
`cat /dev/ttyACM0 |tee -a log_``date +%s\``.nmea`

-----------

Using e.g. android tethering to get "mobile internet" (LTE) to the pi  ...

But if you don't  want to drain your phone battery use something as a "E3272". 
Which works out of the box ... but need a second SIM card ...

-----------

To power the setup. Something like a
"MoPi 2" with 4 * 14650 li-ion batteries (use a BMS protection board!)
should be enough for several houres ...

-----------

Resources:

*  https://www.ardusimple.com
*  https://www.itead.cc/wiki/RPI_Arduino_Sheild_Add-on_V2.0
*  https://www.u-blox.com/en/product/u-center
*  (https://learn.sparkfun.com/tutorials/gps-rtk2-hookup-guide/all)

*  https://www.rtklib.com
*  https://rtkexplorer.com
*  (https://gpsd.gitlab.io/gpsd)
*  (https://github.com/Fraunhofer-IIS/pyUblox)

*  https://www.raspberrypi.org

*  https://cerea-forum.de/forum
*  (https://www.thecombineforum.com)

*  http://sapos.hvbg.hessen.de/service.php

